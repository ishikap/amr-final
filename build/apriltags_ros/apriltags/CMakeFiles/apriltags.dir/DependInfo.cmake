# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/robot3/turtlebot_ws/src/apriltags_ros/apriltags/src/Edge.cc" "/home/robot3/turtlebot_ws/build/apriltags_ros/apriltags/CMakeFiles/apriltags.dir/src/Edge.cc.o"
  "/home/robot3/turtlebot_ws/src/apriltags_ros/apriltags/src/FloatImage.cc" "/home/robot3/turtlebot_ws/build/apriltags_ros/apriltags/CMakeFiles/apriltags.dir/src/FloatImage.cc.o"
  "/home/robot3/turtlebot_ws/src/apriltags_ros/apriltags/src/GLine2D.cc" "/home/robot3/turtlebot_ws/build/apriltags_ros/apriltags/CMakeFiles/apriltags.dir/src/GLine2D.cc.o"
  "/home/robot3/turtlebot_ws/src/apriltags_ros/apriltags/src/GLineSegment2D.cc" "/home/robot3/turtlebot_ws/build/apriltags_ros/apriltags/CMakeFiles/apriltags.dir/src/GLineSegment2D.cc.o"
  "/home/robot3/turtlebot_ws/src/apriltags_ros/apriltags/src/Gaussian.cc" "/home/robot3/turtlebot_ws/build/apriltags_ros/apriltags/CMakeFiles/apriltags.dir/src/Gaussian.cc.o"
  "/home/robot3/turtlebot_ws/src/apriltags_ros/apriltags/src/GrayModel.cc" "/home/robot3/turtlebot_ws/build/apriltags_ros/apriltags/CMakeFiles/apriltags.dir/src/GrayModel.cc.o"
  "/home/robot3/turtlebot_ws/src/apriltags_ros/apriltags/src/Homography33.cc" "/home/robot3/turtlebot_ws/build/apriltags_ros/apriltags/CMakeFiles/apriltags.dir/src/Homography33.cc.o"
  "/home/robot3/turtlebot_ws/src/apriltags_ros/apriltags/src/MathUtil.cc" "/home/robot3/turtlebot_ws/build/apriltags_ros/apriltags/CMakeFiles/apriltags.dir/src/MathUtil.cc.o"
  "/home/robot3/turtlebot_ws/src/apriltags_ros/apriltags/src/Quad.cc" "/home/robot3/turtlebot_ws/build/apriltags_ros/apriltags/CMakeFiles/apriltags.dir/src/Quad.cc.o"
  "/home/robot3/turtlebot_ws/src/apriltags_ros/apriltags/src/Segment.cc" "/home/robot3/turtlebot_ws/build/apriltags_ros/apriltags/CMakeFiles/apriltags.dir/src/Segment.cc.o"
  "/home/robot3/turtlebot_ws/src/apriltags_ros/apriltags/src/TagDetection.cc" "/home/robot3/turtlebot_ws/build/apriltags_ros/apriltags/CMakeFiles/apriltags.dir/src/TagDetection.cc.o"
  "/home/robot3/turtlebot_ws/src/apriltags_ros/apriltags/src/TagDetector.cc" "/home/robot3/turtlebot_ws/build/apriltags_ros/apriltags/CMakeFiles/apriltags.dir/src/TagDetector.cc.o"
  "/home/robot3/turtlebot_ws/src/apriltags_ros/apriltags/src/TagFamily.cc" "/home/robot3/turtlebot_ws/build/apriltags_ros/apriltags/CMakeFiles/apriltags.dir/src/TagFamily.cc.o"
  "/home/robot3/turtlebot_ws/src/apriltags_ros/apriltags/src/UnionFindSimple.cc" "/home/robot3/turtlebot_ws/build/apriltags_ros/apriltags/CMakeFiles/apriltags.dir/src/UnionFindSimple.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "ROS_BUILD_SHARED_LIBS=1"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/opencv"
  "/home/robot3/turtlebot_ws/src/apriltags_ros/apriltags/include"
  "/home/robot3/turtlebot_ws/src/apriltags_ros/apriltags/include/AprilTags"
  "/usr/include/eigen3"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
