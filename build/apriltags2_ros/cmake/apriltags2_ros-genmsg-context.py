# generated from genmsg/cmake/pkg-genmsg.context.in

messages_str = "/home/robot3/turtlebot_ws/src/apriltags2_ros/msg/AprilTagDetectionArray.msg;/home/robot3/turtlebot_ws/src/apriltags2_ros/msg/AprilTagDetection.msg"
services_str = "/home/robot3/turtlebot_ws/src/apriltags2_ros/srv/AnalyzeSingleImage.srv"
pkg_name = "apriltags2_ros"
dependencies_str = "std_msgs;geometry_msgs;sensor_msgs"
langs = "gencpp;genlisp;genpy"
dep_include_paths_str = "apriltags2_ros;/home/robot3/turtlebot_ws/src/apriltags2_ros/msg;std_msgs;/opt/ros/indigo/share/std_msgs/cmake/../msg;geometry_msgs;/opt/ros/indigo/share/geometry_msgs/cmake/../msg;sensor_msgs;/opt/ros/indigo/share/sensor_msgs/cmake/../msg"
PYTHON_EXECUTABLE = "/usr/bin/python"
package_has_static_sources = '' == 'TRUE'
genmsg_check_deps_script = "/opt/ros/indigo/share/genmsg/cmake/../../../lib/genmsg/genmsg_check_deps.py"
