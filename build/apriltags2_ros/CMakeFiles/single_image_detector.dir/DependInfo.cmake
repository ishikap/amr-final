# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/robot3/turtlebot_ws/src/apriltags2_ros/src/single_image_detector.cpp" "/home/robot3/turtlebot_ws/build/apriltags2_ros/CMakeFiles/single_image_detector.dir/src/single_image_detector.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"apriltags2_ros\""
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/robot3/turtlebot_ws/build/apriltags2_ros/CMakeFiles/common.dir/DependInfo.cmake"
  "/home/robot3/turtlebot_ws/build/apriltags2/CMakeFiles/apriltags2.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/robot3/turtlebot_ws/devel/include"
  "/usr/include/opencv"
  "/home/robot3/turtlebot_ws/src/apriltags2_ros/include"
  "/home/robot3/turtlebot_ws/src/apriltags2/include"
  "/opt/ros/indigo/include"
  "/usr/include/eigen3"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
