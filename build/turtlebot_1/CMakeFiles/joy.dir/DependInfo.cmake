# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/robot3/turtlebot_ws/src/turtlebot_1/src/lab1/turtlebot_joy.cpp" "/home/robot3/turtlebot_ws/build/turtlebot_1/CMakeFiles/joy.dir/src/lab1/turtlebot_joy.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"turtlebot_1\""
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/robot3/turtlebot_ws/build/apriltags_ros/apriltags_ros/CMakeFiles/apriltag_detector.dir/DependInfo.cmake"
  "/home/robot3/turtlebot_ws/build/apriltags_ros/apriltags/CMakeFiles/apriltags.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/robot3/turtlebot_ws/devel/include"
  "/home/robot3/turtlebot_ws/src/apriltags_ros/apriltags/include"
  "/home/robot3/turtlebot_ws/src/apriltags_ros/apriltags_ros/include"
  "/opt/ros/indigo/include"
  "/usr/include/eigen3"
  "/usr/include/opencv"
  "/home/robot3/turtlebot_ws/src/turtlebot_1/include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
