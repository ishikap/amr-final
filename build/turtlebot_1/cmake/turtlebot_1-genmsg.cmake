# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "turtlebot_1: 2 messages, 0 services")

set(MSG_I_FLAGS "-Iturtlebot_1:/home/robot3/turtlebot_ws/src/turtlebot_1/msg;-Istd_msgs:/opt/ros/indigo/share/std_msgs/cmake/../msg;-Igeometry_msgs:/opt/ros/indigo/share/geometry_msgs/cmake/../msg")

# Find all generators

add_custom_target(turtlebot_1_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/robot3/turtlebot_ws/src/turtlebot_1/msg/Tag_x.msg" NAME_WE)
add_custom_target(_turtlebot_1_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "turtlebot_1" "/home/robot3/turtlebot_ws/src/turtlebot_1/msg/Tag_x.msg" ""
)

get_filename_component(_filename "/home/robot3/turtlebot_ws/src/turtlebot_1/msg/Tag_y.msg" NAME_WE)
add_custom_target(_turtlebot_1_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "turtlebot_1" "/home/robot3/turtlebot_ws/src/turtlebot_1/msg/Tag_y.msg" ""
)

#
#  langs = 
#


