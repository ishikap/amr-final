# CMake generated Testfile for 
# Source directory: /home/robot3/turtlebot_ws/src
# Build directory: /home/robot3/turtlebot_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(gtest)
SUBDIRS(apriltags_ros/apriltags)
SUBDIRS(apriltags_ros/apriltags_ros)
SUBDIRS(turtlebot_1)
