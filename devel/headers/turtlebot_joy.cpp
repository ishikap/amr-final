// The code below is for the joy stick
#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/Joy.h>
#include <kobuki_msgs/BumperEvent.h>
#include <iostream>
#include <vector>

int pressed = 0;
std::vector<float> direction(8); 
std::vector<int> speed(11); 

void bumperCallback(const kobuki_msgs::BumperEvent::ConstPtr& data)
{
	if(data->state == kobuki_msgs::BumperEvent::PRESSED) {
  		ROS_INFO_STREAM("Pressed");
		pressed = 1;
	}
	else {
  		ROS_INFO_STREAM("Released");
		pressed = 0;
	}
}



void imuCallback(const sensor_msgs::Imu::ConstPtr& data)
{
	std::cout << "Here" << std::endl;
  ROS_INFO("Angular Velocity = %f", data->angular_velocity.z);//print out the angular velocity
}


void joyCallback(const sensor_msgs::Joy::ConstPtr& data)
{
	direction = data->axes; // 
	speed = data->buttons; // 1 or 0

}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "turtlebot_turn");
  ros::NodeHandle nh;
  ros::Publisher vel_pub;
  vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel_mux/input/navi", 1, true);
  ros::Subscriber sub_joy = nh.subscribe("/joy", 10, joyCallback);
  ros::Subscriber sub_bumper = nh.subscribe("/mobile_base/events/bumper", 10, bumperCallback);
  //ros::Subscriber sub_imu = nh.subscribe("/mobile_base/sensors/imu_data", 10, imuCallback);
  ros::Rate loop_rate(10);
  int count = 0;
  geometry_msgs::Twist vel;
  //code added by us
 // kobuki_msgs::BumperEvent msg;
  vel.linear.x = 0.1;//linear velocity(m/s)
  vel.angular.z = 0;//angular velocity(rad/s)
  while (ros::ok())
  {
  	vel_pub.publish(vel);
	
    	ros::spinOnce();
	
    	loop_rate.sleep();
	
	std::cout << direction[0] << " <-d0 d1->  " << direction[1]<< std::endl;
	std::cout << speed[0] << " <-s0 s1->  " << speed[1]<< std::endl;

  	vel.linear.x = 0.5*direction[1];//linear velocity(m/s)
  	vel.angular.z = 0.5*direction[0];//angular velocity(rad/s)
	

  }

  return 0;
}
