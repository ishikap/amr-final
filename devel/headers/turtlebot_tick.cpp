/*

when tick starts to get around 62047 the velocity starts to act funky. The current velocity and the distance both get lower and eventually reach 0. 


*/

// The code below changes the direction of the robot randomly if it bumps into something

#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Imu.h>
#include <kobuki_msgs/BumperEvent.h>
#include <kobuki_msgs/SensorState.h>
#include <sensor_msgs/Joy.h>
#include <iostream>
#include <stdlib.h>
#include <vector>

float vel_current = 0;
int pressed = 0;
float accel = 0;
float speeds[4] = {0.3, 0.5, 0.6, 0};
float tick = 0;
std::vector<float> direction(8); 
std::vector<int> speed(11); 

void tickCallback(const kobuki_msgs::SensorState::ConstPtr& data)
{
  tick  = data->right_encoder;
  //ROS_INFO("Linear  Velocity = %f", data->linear.x);//print out the angular velocity
}

void imuCallback(const sensor_msgs::Imu::ConstPtr& data)
{
  //vel_current = data->linear.x;
  //ROS_INFO("Linear  Velocity = %f", data->linear.x);//print out the angular velocity
}

void bumperCallback(const kobuki_msgs::BumperEvent::ConstPtr& data)
{
	if(data->state == kobuki_msgs::BumperEvent::PRESSED) {
  		ROS_INFO_STREAM("Pressed");
		pressed = 1;
	}
	else {
  		ROS_INFO_STREAM("Released");
		pressed = 0;
	}
}

void joyCallback(const sensor_msgs::Joy::ConstPtr& data)
{
	direction = data->axes; // 
	speed = data->buttons; // 1 or 0

}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "turtlebot_turn");
  ros::NodeHandle nh;
  ros::Publisher vel_pub;
  vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel_mux/input/navi", 1, true);
  ros::Subscriber sub_bumper = nh.subscribe("/mobile_base/events/bumper", 10, bumperCallback);
  ros::Subscriber sub = nh.subscribe("/mobile_base/sensors/imu_data", 10, imuCallback);
  ros::Subscriber sub_tick = nh.subscribe("/mobile_base/sensors/core", 10, tickCallback);
  ros::Subscriber sub_joy = nh.subscribe("/joy", 10, joyCallback);
  float hz = 10;
  float R = 0.038;
  //float N = 2578.33;
  float N = 2570;
  float d = 0;
  float tick_old = 0;

  ros::Rate loop_rate(hz);
  int count = 0;
  float vel_ref = 0.1;
  float vel_error = 0, prev_vel_error = 0;
  float Kp = 0.06;
  float Ki = 0.4;
  float Kd = 0.0003;
  float integral = 0;
  float derivative = 0;
  float output = 0;
  int vel_index = 0;
  float tickmax = 0; // this value has a max limit of 65498
  geometry_msgs::Twist vel;
  vel.linear.x = 0.1;//linear velocity(m/s)
  vel.angular.z = 0;//angular velocity(rad/s)
  //ros::spinOnce();
  //tick_old = tick;
  //loop_rate.sleep(); 
  //std::cout<< "tick from encoder" << tick_old  << std::endl;
  vel_current = 0;
  while (ros::ok())
  {

// is it possible, and do we have to reset the encoder?

	ros::spinOnce();
	
	if(tick > tickmax)
	{
		tickmax = tick;
	}
    	vel_error = vel_ref - vel_current; 
	integral = integral + vel_error/hz;
	derivative = (vel_error-prev_vel_error)*hz;
	output = Kp*vel_error+Ki*integral+Kd*derivative;
	vel.linear.x = output;
	std::cout<< "output = " << output << std::endl;
	if(output > 0 )
	{
  	vel_pub.publish(vel);
	}
	prev_vel_error = vel_error;

    	
	loop_rate.sleep();
	
	d = 2.0*3.14*R*(tick-tick_old)/N;
  	std::cout<< "dist = " << d  << std::endl;
	std::cout<< "tickmax = " << tickmax  << std::endl;
	std::cout<< "tick = " << tick  << std::endl;

	tick_old = tick; 
	vel_current = d*hz;
	std::cout<< "vel_current = " << vel_current << std::endl << std::endl << std::endl << std::endl;

	// if bumper is pressed
	if(pressed==1)
	{
		
		vel.linear.x = -0.1;
		for (int i = 0; i < 5; ++i) 	
		{
    			loop_rate.sleep();
  			vel_pub.publish(vel);
		}
		
		vel.linear.x = 0;
		vel.angular.z = 0.5;

		for (int i = 0; i < (rand()%15 +15); ++i) 	
		{
    			loop_rate.sleep();
  			vel_pub.publish(vel);
		}
		vel.linear.x = 0;
		vel.angular.z = 0;
		vel_index++;
		if(vel_index==3)
		{
			vel_index = 0;
		}
		vel_ref = speeds[vel_index];
	}
  }

  return 0;
}


