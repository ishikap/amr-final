// The code below changes the direction of the robot randomly if it bumps into something

#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Imu.h>
#include <kobuki_msgs/BumperEvent.h>
#include <iostream>
#include <nav_msgs/Odometry.h>
//#include <stdlib.h>
#include <sensor_msgs/LaserScan.h>
#include <vector>

std::vector<float> range_data(640);

int pressed = 0;

float linear_velocity = 0;

void velCallback(const nav_msgs::Odometry::ConstPtr& data)
{
  	linear_velocity = data->twist.twist.linear.x;
}


void scanCallback(const sensor_msgs::LaserScan::ConstPtr& data)
{
 	range_data = data-> ranges;//[0-639];//[0-639];
	ROS_INFO("position 320 = %f ",range_data[320]);
}



int main(int argc, char **argv)
{
  ros::init(argc, argv, "turtlebot_turn");
  ros::NodeHandle nh;
  ros::Publisher vel_pub;
  vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel_mux/input/navi", 1, true);
  ros::Subscriber sub_vel = nh.subscribe("/odom", 10, velCallback);
  ros::Subscriber sub_scan = nh.subscribe("/scan", 10, scanCallback);
  ros::Rate loop_rate(10);
  int count = 0;
  int alpha = 0;
  geometry_msgs::Twist vel;
  vel.linear.x = 0;//.1;//linear velocity(m/s)
  vel.angular.z = 0;//angular velocity(rad/s)
  
  float vg = 0.6;
  float vc = 0.0;
  // Gains
  float kp = 0.9;
  float ki = 0.9;
  float kd = 0.1;
  //float ka = 0.7;

  //misc.
  float derror = 0; //dist
  float terror = 0; //theta
  float perror = 0; //prev_velocity
  float verror = 0; //velocity
 
// 
  float dt = 1.0/10.0;
  //
  float integral = 0;
  float derivative = 0;
  float gamma = 0;
  float thetad = 0;
  float acc = 0;
  
  while (ros::ok())
  {
	
  	vel_pub.publish(vel);
    	ros::spinOnce();
	

	//derror = sqrt( ((xg-xc)*(xg-xc)) + ((yg-yc)*(yg-yc)) );
		
	if(range_data[320] <= 1)
	{
		vel.linear.x = 0;
  		vel.angular.z = 0;//angular velocity(rad/s)
	}
	else {

		//vc = linear_velocity;
		// PID
		/*
        	verror = vg - vc;
		integral += verror * dt;
		derivative = (verror-perror)/dt;
		acc = (kp*verror) + (ki*integral) + (kd*derivative);
        	perror = verror;
		vc += acc*dt;
		if(vc > 0.65)
		{
			vc = 0.65;
		}
		*/
		
		vel.linear.x = 0.2;
  		vel.angular.z = 0;//angular velocity(rad/s)
	}

    	loop_rate.sleep();
	
  }

  return 0;
}


