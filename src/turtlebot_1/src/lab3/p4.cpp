// The code below changes the direction of the robot randomly if it bumps into something

#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/Image.h>
#include <kobuki_msgs/BumperEvent.h>
#include <iostream>
#include <nav_msgs/Odometry.h>
//#include <stdlib.h>
#include <sensor_msgs/LaserScan.h>
#include <vector>

bool checker(int r, int g, int b); 
bool detection_logic(int a, int b); 

std::vector<float> range_data(640);


int pressed = 0;
float mins[32];
float linear_velocity = 0;
float smallest = 10;
int smallest_i = -10;
std::vector<unsigned char> image;
int image_height;
int image_width;
int image_step;
std::vector<std::vector<int> > R;
std::vector<std::vector<int> > G;
std::vector<std::vector<int> > B;



void velCallback(const nav_msgs::Odometry::ConstPtr& data)
{

  	linear_velocity = data->twist.twist.linear.x;
}

void imageCallback(const sensor_msgs::Image::ConstPtr& data)
{

  	image_height = data->height;
	image_width = data->width;
	image_step = data->step;

	//image.resize(image_step, std::vector<int>(image_height));
	image = data->data;

	//Resizing channels;
	R.resize(image_width, std::vector<int>(image_height));
	G.resize(image_width, std::vector<int>(image_height));
	B.resize(image_width, std::vector<int>(image_height));

	size_t l = 0;
	for(size_t i = 0; i < image_height; ++i) {
		for(size_t j = 0; j < image_width; ++j ) {
			R[i][j] = image[l++];
			G[i][j] = image[l++];
			B[i][j] = image[l++];
		}	
	}
}

void scanCallback(const sensor_msgs::LaserScan::ConstPtr& data)
{
 	range_data = data-> ranges;//[0-639];//[0-639];
//	ROS_INFO("position 320 = %f ",range_data[320]);
	for (int min_index = 0; min_index < 32; min_index++){
		mins[min_index] = 10;
		for(int range_index = min_index*20; range_index < min_index*20+19; range_index++){
			if(range_data[range_index]<mins[min_index]){
				mins[min_index]=range_data[range_index];
			}
		}

		if(min_index != 0 && min_index != 29){
//			ROS_INFO("index = %i, value = %f ",min_index,mins[min_index]);
		}
	}

	smallest = 10;
	smallest_i = -10;

	for(int i = 0; i <= 31; i++){
		if(mins[i] == 10){
			//std::cout<< "HEREEEEEEEEEE\n";
			//mins[i] = 0;
		}
		if(i != 0 && i != 29){
			if(mins[i]< smallest){
				smallest = mins[i];
				smallest_i = i;
			}
		}
	}
	ROS_INFO("smallest index = %i, smallest value = %f ",smallest_i,smallest);
}



int main(int argc, char **argv)
{
  ros::init(argc, argv, "turtlebot_turn");
  ros::NodeHandle nh;
  ros::Publisher vel_pub;
  vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel_mux/input/navi", 1, true);
  ros::Subscriber sub_vel = nh.subscribe("/odom", 10, velCallback);
  ros::Subscriber sub_scan = nh.subscribe("/scan", 10, scanCallback);
  ros::Subscriber sub_image = nh.subscribe("/camera/rgb/image_rect_color", 10, imageCallback);
  ros::Rate loop_rate(10);
  int count = 0;
  int alpha = 0;
  geometry_msgs::Twist vel;
  vel.linear.x = 0.0;//.1;//linear velocity(m/s)
  vel.angular.z = 0;//angular velocity(rad/s)
  
  float vg = 0.6;
  float vc = 0.0;
  // Gains
  float kp = 0.9;
  float ki = 0.9;
  float kd = 0.1;
  //float ka = 0.7;

  //misc.
  float derror = 0; //dist
  float terror = 0; //theta
  float perror = 0; //prev_velocity
  float verror = 0; //velocity
 
// 
  float dt = 1.0/10.0;
  //
  float integral = 0;
  float derivative = 0;
  float gamma = 0;
  float thetad = 0;
  float acc = 0;
  float ok_dist = 0.8;

  float pls_stop = 0.4;
  int small_i = 15;
  
  while (ros::ok())
  {
	
  	vel_pub.publish(vel);
    	ros::spinOnce();
		/*
	if(smallest_i<12)
	{
  		vel.angular.z = -1;//angular velocity(rad/s)
	}
	else if(smallest_i>19) 
	{
  		vel.angular.z = 1;//angular velocity(rad/s)
	}
	else
	{ 
		vel.angular.z = 0;
	}


	if(smallest < ok_dist) 
	{
		vel.linear.x = -0.2;
	}
	else if(smallest >= ok_dist && smallest < (ok_dist+pls_stop))
	{	
		vel.linear.x = 0;
	} 
	else if(smallest >= (ok_dist+pls_stop))
	{
		vel.linear.x = 0.2;
	}

	

	

	if(smallest < ok_dist)
	{
		vel.linear.x = stop_pls;
		stop_pls = stop_pls - 0.05;
		if(stop_pls <=0){
			stop_pls = 0;
		}
	}
	else if(smallest >= ok_dist)
	{
		vel.linear.x = 0.2;
		stop_pls = 0.2;
	}
	*//*
	for(size_t i = 0; i < image_height; ++i) {
			std::cout<<image[i]<<" ";
			std::cout<<"\n";
	}

	std::cout<<
	*/
	/*
	std::cout<<image.size()<<"\n";
	std::cout<<image_height<<"\n";
	std::cout<<image_width<<"\n";
	std::cout<<image_step<<"\n";
	  */

	//DETECTION LOGIC -------------------***********--------------------  
	int start_i = 0;
	int start_j = 0;
	bool detected = false;
	for(size_t i = 0; i < image_height; ++i) {
		for(size_t j = 0; j < image_width; ++j) {
			if(checker(R[i][j], G[i][j], B[i][j])) {
				if(detection_logic(i, j)) {
					detected = true;
					small_i = j/20;
					start_i = i;
					start_j = j;
					break;				
				}
			}
		}
		if(detected) break;
	}

		
	std::cout << "i: "<< start_i <<"\n";
	std::cout << "j: "<< small_i<<"\n";

	loop_rate.sleep();



	if(small_i<12)
	{
  		vel.angular.z = 0.5;//angular velocity(rad/s)
	}
	else if(small_i>17) 
	{
  		vel.angular.z = -0.5;//angular velocity(rad/s)
	}
	else
	{ 
		vel.angular.z = 0;
	}


	if(smallest < ok_dist) 
	{
		vel.linear.x = -0.1;
	}
	else if(smallest >= ok_dist && smallest < (ok_dist+pls_stop))
	{	
		vel.linear.x = 0;
	} 
	else if(smallest >= (ok_dist+pls_stop))
	{
		vel.linear.x = 0.1;
	}


  }

  return 0;
}

bool checker(int r, int g, int b) 
{
	return (r>220 && r<256 && g>-1 && g<50 && b>-1 && b<50);
}

bool detection_logic(int a, int b) 
{
	for(int i = a; i<a+2; ++i) {
		for(int j = b; j <b+2; ++j) {
			if(!checker(R[i][j], G[i][j], B[i][j])) {
				return false;
			}
		}
	}
	return true;
}
