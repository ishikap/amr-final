#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/LaserScan.h>
#include <math.h>

std::vector<float> range_data(640);
float smallest = 10;
int smallest_i = -10;
float largest = 0;
int largest_i = 0;
float mins[32];

float pose_x = 0;
float pose_y = 0;
float orient = 0;

void poseCallback(const nav_msgs::Odometry::ConstPtr& data)
{
  	pose_x = data->pose.pose.position.x;
  	pose_y = data->pose.pose.position.y;
	orient = data->pose.pose.orientation.z;
}

void scanCallback(const sensor_msgs::LaserScan::ConstPtr& data)
{

 	range_data = data-> ranges;//[0-639];//[0-639];
//	ROS_INFO("position 320 = %f ",range_data[320]);
	for (int min_index = 0; min_index < 32; min_index++){
		mins[min_index] = 10;
		for(int range_index = min_index*20; range_index < min_index*20+19; range_index++){
			if(range_data[range_index]<mins[min_index]){
				mins[min_index]=range_data[range_index];
			}
		}

		if(min_index != 0 && min_index != 29){
//			ROS_INFO("index = %i, value = %f ",min_index,mins[min_index]);
		}
	}

	smallest = 10;
	smallest_i = -10;

	for(int i = 0; i <= 31; i++){
		if(mins[i] == 10){
			//std::cout<< "HEREEEEEEEEEE\n";
			//mins[i] = 0;
		}
		if(i != 0 && i != 29){
			if(mins[i]< smallest){
				smallest = mins[i];
				smallest_i = i;
			}
		}
	}
	ROS_INFO("smallest index = %i, smallest value = %f ",smallest_i,smallest);

/*
 	range_data = data-> ranges;//[0-639];//[0-639];
	//ROS_INFO("position 320 = %f ",range_data[320]);
	for (int min_index = 0; min_index < 32; min_index++){
		mins[min_index] = 10;
		for(int range_index = min_index*20; range_index < min_index*20+19; range_index++){
			if(range_data[range_index]<mins[min_index]){
				mins[min_index]=range_data[range_index];
			}
		}

		if(min_index != 0 && min_index != 29){
//			ROS_INFO("index = %i, value = %f ",min_index,mins[min_index]);
		}
	}

	smallest = 10;
	smallest_i = -10;
	largest = 0;
	largest_i = -10;

	for(int i = 0; i < 32; i++){
		if(mins[i] == 10){
			//mins[i] = 0;
		}

		if(i != 0 && i != 29){
			if(mins[i]< smallest){
				smallest = mins[i];
				smallest_i = i;
			}
		}

		if(i != 0 && i != 29){
			if(mins[i]> largest){
				largest = mins[i];
				largest_i = i;
			}
		}
		
	}
	ROS_INFO("smallest index = %i, smallest value = %f ",smallest_i,smallest);
*/
}

void imuCallback(const sensor_msgs::Imu::ConstPtr& data)
{
  // ROS_INFO("Angular Velocity = %f", data->angular_velocity.z);//print out the angular velocity
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "turtlebot_pose");
  ros::NodeHandle nh;
  ros::Publisher vel_pub;
  vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel_mux/input/navi", 1, true);
  ros::Subscriber sub_pose = nh.subscribe("/odom", 1000, poseCallback);
  ros::Subscriber sub = nh.subscribe("/mobile_base/sensors/imu_data", 10, imuCallback);
  ros::Subscriber sub_scan = nh.subscribe("/scan", 10, scanCallback);
  float hz = 10;
  ros::Rate loop_rate(hz);
  int count = 0;
  geometry_msgs::Twist vel;
  vel.linear.x = 0.1;//linear velocity(m/s)
  vel.angular.z = 0;//angular velocity(rad/s)

  // problem 1 lab 2 code below
  float xc = 0;
  float yc = 0;
  float thetac = 0;
  int temp_fix = 0;
  
  // goal positions
  float xg = 4;
  float yg = 0;

  // Gains
  float kv = 0.1;
  float kp = 1.5;//0.5;//0.6

  // misc. 
  float dt = 1.0/hz;
  float derror = 0;
  float terror = 0;
  float gamma = 0;
  float thetad = 0;

  float x_corrector = 0;
  float y_corrector = 0;

  bool start = false;
  bool threshold = false;
  int obstacle_detected_count = 0;
  int sec_hold = 9;
  while (ros::ok())
  {
	vel_pub.publish(vel);
    	ros::spinOnce();
	derror = sqrt( ((xg-xc)*(xg-xc)) + ((yg-yc)*(yg-yc)) );

	std::cout <<"SMALLESSTTTTTTT"<< smallest<< std::endl;
	// obstacle avoidance behavior //////////////////////////////////////////
	if(smallest<=1) 
	{
		std::cout<<"OBSTACLEEEEEEEEEEEEEEEEEEE\n";
		vel.linear.x = 0.1;
		
        	if(smallest_i<15)
		{
			gamma =1.1;
		}
		else if(smallest_i>16)
		{
			gamma =-1.1;
                }
		else {
			gamma = 1.1;
		}

		//gamma = -1;
		temp_fix = 1;
	  	kp = 1.5;//0.5;//0.6
		count = 0;

	}    	
	else 
	{
		// go to goal behavior //////////////////////////////////////////
		// distance correction
		//else if(derror < mins[15] && derror < mins[31] && derror < mins[3])
		std::cout<<"GTGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG\n";

		if(temp_fix == 1)
		{
			count++;
			if(count >= hz*sec_hold)
			{
				kp = 1.5;
				temp_fix = 0;
				count = 0;
			}
	/*
			temp_fix = 0;
			for(int l = 0; l < 50; l++)
			{			
				vel_pub.publish(vel);
				ros::spinOnce();
				loop_rate.sleep();
			}
	*/
		}

		vel.linear.x  = kv * derror; //* dt *10;
		if(vel.linear.x > 0.1)
		{
			vel.linear.x = 0.1;
		}
		// angle correction
		thetad = atan2((yg-yc), (xg-xc));
		terror = thetad - thetac;
		gamma = kp * atan2(sin(terror), cos(terror));
	}
	ROS_INFO("--------------------------------dist error = %f, mins[15] = %f ",derror,mins[15]);

/*
		
	if(range_data[320]<=1) 
	{
		threshold = 1;
		gamma = 1;
		vel.linear.x = 0;
	}
	if(threshold == 1) {
		gamma = 1;
		vel.linear.x = 0;
		if(range_data[320]>=2)
		{
			threshold = 0;
		}
	}
*/
	vel.angular.z = gamma ;//* dt;

	//update position
	//thetac += orient;
	thetac += vel.angular.z*dt;
	xc = pose_x;
	yc = pose_y;

	//std::cout<<"data from odom x: "<< pose_x<<std::endl;
	//std::cout<<"data from odom y: "<< pose_y<<std::endl;

	
	
	std::cout<<"speed "<<vel.linear.x<<std::endl;
	std::cout<<"x "<<xc<<std::endl;	
	std::cout<<"y "<<yc<<std::endl;	
	std::cout<<"t "<<thetac<<std::endl;	
	std::cout<<"dist error " <<derror<<std::endl;	

	if(derror < 0.2) 
	{
		return 0;
	}	
	
	loop_rate.sleep();
	
  }

  return 0;
}
