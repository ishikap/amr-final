#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Imu.h>
#include <math.h>

float pose_x = 0;
float pose_y = 0;

void poseCallback(const nav_msgs::Odometry::ConstPtr& data)
{
  	pose_x = data->pose.pose.position.x;
  	pose_y = data->pose.pose.position.y;
}


void imuCallback(const sensor_msgs::Imu::ConstPtr& data)
{
  ROS_INFO("Angular Velocity = %f", data->angular_velocity.z);//print out the angular velocity

}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "turtlebot_pose");
  ros::NodeHandle nh;
  ros::Publisher vel_pub;
  vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel_mux/input/navi", 1, true);
  ros::Subscriber sub_pose = nh.subscribe("/odom", 1000, poseCallback);
  ros::Subscriber sub = nh.subscribe("/mobile_base/sensors/imu_data", 10, imuCallback);
  float hz = 10;
  ros::Rate loop_rate(hz);
  int count = 0;
  geometry_msgs::Twist vel;
  vel.linear.x = 0;//linear velocity(m/s)
  vel.angular.z = 0;//angular velocity(rad/s)

  // problem 1 lab 2 code below
  float xc = 0;
  float yc = 0;
  float thetac = 0;
  
  // goal positions
  float xg = 1.5;
  float yg = 1.5;

  // Gains
  float kv = 0.15;
  float kp = 0.65;

  // misc. 
  float dt = 1.0/hz;
  float derror = 0;
  float terror = 0;
  float gamma = 0;
  float thetad = 0;

  float x_corrector = 0;
  float y_corrector = 0;

  bool start = true;
	
  while (ros::ok())
  {
	vel_pub.publish(vel);
    	ros::spinOnce();
	loop_rate.sleep();
	    	
	// real code
	// distance correction
	derror = sqrt( ((xg-xc)*(xg-xc)) + ((yg-yc)*(yg-yc)) );
	vel.linear.x  = kv * derror; //* dt *10;
	if(vel.linear.x > 0.65)
	{
		vel.linear.x = 0.65;
	}

	// angle correction
		thetad = atan2((yg-yc), (xg-xc));
		terror = thetad - thetac;
		gamma = kp * atan2(sin(terror), cos(terror));
		vel.angular.z = gamma ;//* dt;
	

	//update position
	thetac += vel.angular.z*dt;
	
	xc = pose_x;
	yc = pose_y;

	std::cout<<"data from odom x: "<< pose_x<<std::endl;
	std::cout<<"data from odom y: "<< pose_y<<std::endl;

	
	
	std::cout<<"speed "<<vel.linear.x<<std::endl;
	std::cout<<"x "<<xc<<std::endl;	
	std::cout<<"y "<<yc<<std::endl;	
	std::cout<<"t "<<thetac<<std::endl;	
	std::cout<<"dist error " <<derror<<std::endl;	

	if(derror < 0.05) 
	{
		return 0;
	}	

	std::cout<<"vel_pub        "<<vel_pub<<std::endl;
	
	
  }

  return 0;
}
