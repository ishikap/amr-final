#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include <kobuki_msgs/CliffEvent.h>
#include <kobuki_msgs/SensorState.h>
#include <iostream>
#include <sensor_msgs/Imu.h>
#include <kobuki_msgs/BumperEvent.h>

int senseL=1600;
int senseC=1800;
int senseR=1600;

//int bottom[3] = {0,0,0};
int bottom[3] = {0,0,0};
int cliff = 0;

int pressed = 0;

void bumperCallback(const kobuki_msgs::BumperEvent::ConstPtr& data)
{
	if(data->state == kobuki_msgs::BumperEvent::PRESSED) {
  		ROS_INFO_STREAM("Pressed");
		pressed = 1;
	}
	else {
  		ROS_INFO_STREAM("Released");
		pressed = 0;
	}
}

// the values below provide the typical values from the floor, and the minimum threshold we should set for when we see a line
void cliffCallback (const kobuki_msgs::SensorState::ConstPtr& data)
{
	senseR = data->bottom[0];
	senseC = data->bottom[1];
	senseL = data->bottom[2];
}


void imuCallback(const sensor_msgs::Imu::ConstPtr& data)
{
  //ROS_INFO("Angular Velocity = %f", data->angular_velocity.z);//print out the angular velocity
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "turtlebot_turn");
  ros::NodeHandle nh;
  ros::Publisher vel_pub;
  vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel_mux/input/navi", 1, true);
  ros::Subscriber sub = nh.subscribe("/mobile_base/sensors/imu_data", 10, imuCallback);
  ros::Subscriber sub1 = nh.subscribe("/mobile_base/sensors/core", 10, cliffCallback);
  ros::Subscriber sub_bumper = nh.subscribe("/mobile_base/events/bumper", 10, bumperCallback);
//  ros::Subscriber sub1 = nh.subscribe("/mobile_base/events/cliff", 10, cliffCallback);
  float hz = 100;
  ros::Rate loop_rate(hz);
  int count = 0;
  ros::spinOnce();
  // threshold, when the sensors hit these values, the robot may turn depending on the condition
  int lineL = 1530;//1607newa-1547nmin //1540;//slow	// on floor --> // 1685 max // 1597 avg // 1549 min --- Black tape -->
  int lineC = 1750;//1827newa-1773nmin //1750;//slow	// on floor --> // 1864 max // 1822 avg // 1773 min --- Black tape -->
  int lineR = 1550;//1624newa-1597nmin //1560;//slow	// on floor --> // 1677 max // 1620 avg // 1574 min --- Black tape --> 

  // angular speed
  float omega = 0.8;//0.7
  float lineVel = 0.32;//0.25;//0.32;//0.32 with 0.3 comp
  float comp_omega = 0.5;//0.3 // the ratio from comp_omega to lineVel is 1
  float turnVel = 0.15; //<-- this value works
	//In front course 0.32, with 0.5 overcompensation works well!!
	//In zig zag course if comp_omega == line_vel works well!! 

  int turning = 0;
  geometry_msgs::Twist vel;
  vel.linear.x = lineVel;//linear velocity(m/s)
  vel.angular.z = 0;//angular velocity(rad/s)

  int maxL = 0;
  int minL = 2000;

  int maxC = 0;
  int minC = 2000;

  int maxR = 0;
  int minR = 2000;

  int avgL = 1600;
  int avgC = 1800;
  int avgR = 1600;
  int idk = 0;
  int loop = 0;
  float turnTiming = 0;
  int alt = 0;
  int angle = 90;

  while (ros::ok())
  {
	loop++;
    	vel_pub.publish(vel);
    	ros::spinOnce();
    	loop_rate.sleep();
	
	if(pressed==1)
	{
		vel.linear.x = 0;
		vel_pub.publish(vel);
		while(1){/*nothing*/
    			ros::spinOnce();
    			loop_rate.sleep();
			return 0;
		}

	}
	//////////////////////////////////////////////////////////////////////
	//troubleshooting code below by chris///////////////////////////////////	
	//code below captures average for cliff sensors

	if(senseL>maxL)
	{
		maxL=senseL;
		ROS_INFO("Left max = %i-------------------------",maxL);
	}
	else if(senseL<minL)
	{
		minL=senseL;
		ROS_INFO("Left min = %i-------------------------",minL);
	}

	if(senseC>maxC)
	{
		maxC=senseC;
		ROS_INFO("--------------Center max = %i-----------",maxC);
	}
	else if(senseC<minC)
	{
		minC=senseC;
		ROS_INFO("--------------Center min = %i-----------",minC);	
	}
	
	if(senseR>maxR)
	{
		maxR=senseR;
		ROS_INFO("-------------------------Right max = %i",maxR);
	}
	else if(senseR<minR)	
	{
		minR=senseR;
		ROS_INFO("-------------------------Right min = %i",minR);
	}


	//code below captures max for cliff sensors	
/*
	avgL+=senseL;
	idk = avgL/loop;
	ROS_INFO("Left avg =%i--------------------------",idk);//senseL);//idk);//
	avgC+=senseC;
	idk = avgC/loop;
	ROS_INFO("--------------Center avg=%i-----------",idk);//senseC);//idk);
	avgR+=senseR;
	idk = avgR/loop;
	ROS_INFO("--------------------------Right avg=%i",idk);//senseR);//idk);
*/

	//troubleshooting code above by chris///////////////////////////////////
	//////////////////////////////////////////////////////////////////////
	

	// main if statements below
	if(senseL <= lineL && turning == 0)
	{
		vel.angular.z = omega;
		vel.linear.x = turnVel;
		ROS_INFO("Left=%i-------------------------",senseL);
		turning = 1;
	}
	else if(senseR <= lineR && turning == 0)
	{
		vel.angular.z = -omega;
		vel.linear.x = turnVel;
		ROS_INFO("-------------------------Right=%i",senseR);
		turning = 1;
	}
	else if(senseC <= lineC && vel.angular.z == 0)
	{
		vel.angular.z = 0;
		vel.linear.x = lineVel;
		ROS_INFO("-----------Center=%i--------------",senseC);
	}
	else if(senseC <= lineC && vel.angular.z != 0 && turning == 1)
	{
		if (vel.angular.z > 0)
			vel.angular.z = -comp_omega;
		else
			vel.angular.z = comp_omega;

		vel.linear.x = lineVel;
		ROS_INFO("-----------Center=%i--------------",senseC);
		turning = 0;
	}
	
	//*
	if(turning==1)
	{
		turnTiming+=1;
		if(turnTiming >= (200/30*angle))
		{
			vel.angular.z = -vel.angular.z;
			vel.linear.x = 0;//-0.01;
			turnTiming = 0;
			ROS_INFO("--------WORKING!!!!--------------");
			if(alt == 0)
				angle = 180;
				
		}
	}
	else
	{
	   	turnTiming = 0;
		angle = 90;
		alt = 0;
	}
//	*/
  }
  return 0;
}
