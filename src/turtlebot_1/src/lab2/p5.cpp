#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Imu.h>
#include <math.h>
#include <vector>

float pose_x = 0;
float pose_y = 0;
float orient = 0;

void poseCallback(const nav_msgs::Odometry::ConstPtr& data)
{
  	pose_x = data->pose.pose.position.x;
  	pose_y = data->pose.pose.position.y;
	orient = data->pose.pose.orientation.z;
}


void imuCallback(const sensor_msgs::Imu::ConstPtr& data)
{
  ROS_INFO("Angular Velocity = %f", data->angular_velocity.z);//print out the angular velocity

	//std::cout<<"HERE\n";
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "turtlebot_pose");
  ros::NodeHandle nh;
  ros::Publisher vel_pub;
  vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel_mux/input/navi", 1, true);
  ros::Subscriber sub_pose = nh.subscribe("/odom", 1000, poseCallback);
  ros::Subscriber sub = nh.subscribe("/mobile_base/sensors/imu_data", 10, imuCallback);
  float hz = 10;
  ros::Rate loop_rate(hz);
  int count = 0;
  geometry_msgs::Twist vel;
  float velL = 0.2;
  float velA = 0.1;
  vel.linear.x = velL;//linear velocity(m/s)
  vel.angular.z = 0;//angular velocity(rad/s)

  // problem 1 lab 2 code below
  float xc = 0;
  float yc = 0;
  float thetac = 0;
  float xval = 1.2;
  float yval = xval;

  // goal positions
  float xg_vec[] = {xval, xval, 0, 0};
  float yg_vec[] = {0, yval, yval, 0};
  float xg = xg_vec[0];
  float yg = yg_vec[0];


  // delta 
  float dt = 1.0/hz;
  

  //velocities
  float vg = 0.1;
  float vc = 0.0;

  int countA = 0;
  int countL = 0;
  int squareSize = 75; // squareSize*dt*velL = how far you went
  int angleSize = 180; // angleSize*dt*velA = how much you turn
  //visited counter
  int visited = 0;
  int turning = 0;
  float quat[] = {0.7, 1, -0.7, 0};
  float quatc = quat[0];

  while (ros::ok())
  {
 	std::cout<<"finally at goal number: "<<visited<< std::endl;	
	
	vel_pub.publish(vel);
    	ros::spinOnce();
	loop_rate.sleep();
	    
	++countL;
	if(countL == squareSize && turning == 0)
	{
		vel.linear.x = 0;
		//countL = 0;
		countA = 0;
		vel.angular.z = velA;
		turning = 1;
		if(visited == 3)
			return 0;
	}
	if( turning  && (orient<(quatc+0.005) && orient>(quatc-0.005)))
	{
		vel.angular.z = 0;
		countL = 0;
		vel.linear.x = velL;
		turning = 0;
		++visited;
		quatc = quat[visited];
	}
 
  }

  return 0;
}
