#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Imu.h>
#include <math.h>
#include <vector>
#include <fstream>

float pose_x = 0;
float pose_y = 0;
float linear_velocity = 0;

void poseCallback(const nav_msgs::Odometry::ConstPtr& data)
{
  	pose_x = data->pose.pose.position.x;
  	pose_y = data->pose.pose.position.y;
}

void velCallback(const nav_msgs::Odometry::ConstPtr& data)
{
  	linear_velocity = data->twist.twist.linear.x;
}


void imuCallback(const sensor_msgs::Imu::ConstPtr& data)
{
  ROS_INFO("Angular Velocity = %f", data->angular_velocity.z);//print out the angular velocity

	//std::cout<<"HERE\n";
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "turtlebot_pose");
  ros::NodeHandle nh;
  ros::Publisher vel_pub;
  vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel_mux/input/navi", 1, true);
  ros::Subscriber sub_pose = nh.subscribe("/odom", 1000, poseCallback);
  ros::Subscriber sub_vel = nh.subscribe("/odom", 1000, velCallback);
  ros::Subscriber sub = nh.subscribe("/mobile_base/sensors/imu_data", 10, imuCallback);
  float hz = 10;
  ros::Rate loop_rate(hz);
  int count = 0;
  geometry_msgs::Twist vel;
  vel.linear.x = 0;//linear velocity(m/s)
  vel.angular.z = 0;//angular velocity(rad/s)

  //nh.reset_odometry();
  // problem 1 lab 2 code below
  float xc = 0;
  float yc = 0;
  float thetac = 0;

  float r = 1;
  std::vector<float> xg_vec;
  std::vector<float> yg_vec;
  float n = 1;
  
  std::ofstream bag;
  bag.open("heart_theory.csv");
  std::ofstream data;
  data.open("heart_prac.csv");
  float t = 0;
  for(int i = 0; i <16; ++i )
  {
	float tempy = sin(t)*sin(t)*sin(t);
	float tempx = cos(t) - 0.5*cos(2*t) - 0.2*cos(3*t) - 0.01*cos(4*t);
	
	xg_vec.push_back(tempx);
	yg_vec.push_back(tempy);
	std::cout<<"x "<<xg_vec.back()<<std::endl;
	std::cout<<"y "<<yg_vec.back()<<std::endl;

	bag<<xg_vec.back()<<","<<yg_vec.back()<<std::endl;
        t += (3.14/8); 
  }

/*
  n = (-1*r)+0.2;
  for(int i = 0; i < 10; ++i)
  {
	xg_vec.push_back(n);
	float temp = -3*sqrt(1 - (sqrt(sqrt(n*n))/sqrt(2)));
	if(0.5 < (sqrt(sqrt(n*n))))
		temp = 0;
	yg_vec.push_back(temp);
	n += 0.2;
	std::cout<<"x " <<xg_vec.back()<<std::endl;
	std::cout<<"y" <<yg_vec.back()<<std::endl;
  }
*/
//  float xval = 1.2;
//  float yval = xval;



  // goal positions
  //float xg_vec[] = {xval, xval, 0, 0};
  //float yg_vec[] = {0, yval, yval, 0};
  float xg = xg_vec.back();
  float yg = yg_vec.back();
  xg_vec.pop_back();
  yg_vec.pop_back();


  // Gains
  float kp = 1;
  float ki = 0.00;
  float kd = 0.1;
  float ka = 0.25;
  float kai= 0.002;

  // delta 
  float dt = 1.0/hz;
  

  //velocities
  float vg = 0.1;
  float vc = 0.0;


  //misc.
  float derror = 0; //dist
  float terror = 0; //theta
  float perror = 0; //prev_velocity
  float verror = 0; //velocity
  
  //
  float integral = 0;
  float integral_ang = 0;
  float derivative = 0;
  float gamma = 0;
  float thetad = 0;
  float acc = 0;

  //visited counter
  int visited = 0;

  while (ros::ok())
  {
 	std::cout<<"finally at goal number: "<<visited<< std::endl;	
 	std::cout<<"finally at goal number: "<<visited<< std::endl;	
	
	vel_pub.publish(vel);
    	ros::spinOnce();
	loop_rate.sleep();
	    	
	// real code
	// distance correction
	derror = sqrt( ((xg-xc)*(xg-xc)) + ((yg-yc)*(yg-yc)) );
	

	vc = linear_velocity;
	// PID
        verror = vg - vc;
	integral += verror * dt;
	derivative = (verror-perror)/dt;
	acc = (kp*verror) + (ki*integral) + (kd*derivative);
        perror = verror;


	vc += acc*dt;
	
	//vel.linear.x  = kv * derror; /
	if(vc > 0.65)
	{
		vc = 0.65;
	}
	vel.linear.x = vc;

	// angle correction
		thetad = atan2((yg-yc), (xg-xc));
		terror = thetad - thetac;
		integral_ang = terror*dt;
		gamma = ka * atan2(sin(terror), cos(terror)) + ki * atan2(sin(integral_ang),cos(integral_ang));
		vel.angular.z = gamma ;
	

	//update position
	thetac += vel.angular.z*dt;
	xc = pose_x;
	yc = pose_y;

	std::cout<<"data from odom x: "<< pose_x<<std::endl;
	std::cout<<"data from odom y: "<< pose_y<<std::endl;
	
	std::cout<<"HEYYYYYYYY IM the SPEED: "<< vc<<std::endl;
	std::cout<<"GOALLLL XXXX: "<< xg<<std::endl;
	std::cout<<"GOALLLL YYYY: "<< yg<<std::endl;

	
	
	//vel_pub.publish(vel);
	std::cout<<"speed "<<vel.linear.x<<std::endl;
	std::cout<<"x "<<xc<<std::endl;	
	std::cout<<"y "<<yc<<std::endl;	
	std::cout<<"t "<<thetac<<std::endl;	
	std::cout<<"dist error " <<derror<<std::endl;	
	
	data<<xc<<","<<yc<<std::endl;

	if(derror < 0.37) 
	{
		++visited;
		if(xg_vec.empty()){
		   	return 0;	
		}
		xg = xg_vec.back();
		yg = yg_vec.back();
  		xg_vec.pop_back();
  		yg_vec.pop_back();
	}	
	
	
  }
  bag.close();
  data.close();
  return 0;
}
