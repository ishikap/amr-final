#include "ros/ros.h"
#include <std_msgs/Float64.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Imu.h>
#include <kobuki_msgs/BumperEvent.h>
#include <iostream>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/LaserScan.h>
#include <vector>
#include <math.h>
#include <utility>
#include <apriltags_ros/AprilTagDetection.h>
#include <apriltags_ros/AprilTagDetectionArray.h>



// raw data from laser scan
std::vector<float> range_data(640);

// April Tags read
std::vector<std_msgs::Float64> tags_x(5);
std::vector<std_msgs::Float64> tags_y(5);
// minimum range detected
float min_range = 100;
int min_index = 0;
int min_sector = 0;

float orientation_z = 0;
std::vector<apriltags_ros::AprilTagDetection> detections;


void odom_call_back(const nav_msgs::Odometry::ConstPtr& data)
{

  	orientation_z = data->pose.pose.orientation.z;
}

void tag_call_back(const apriltags_ros::AprilTagDetectionArray::ConstPtr& data)
{

  	detections = data->detections;
}

void scanCallback(const sensor_msgs::LaserScan::ConstPtr& data)
{
	// get data from laser scan
 	range_data = data->ranges;//[0-639]
	
	min_range = 100;
	min_index = 0;
	min_sector = 0;
	// find global minimum
	for(size_t i = 0; i < 640; ++i)
	{
		if(range_data[i]<min_range)
		{
			min_range = range_data[i];
			min_index = i;
		}
	
	}
	int sector_width = 20;
	// therefore, there are 32 sectors
	min_sector = floor(min_index/sector_width);
}


int main(int argc, char **argv)
{
	ros::init(argc, argv, "turtlebot_tag_detection");
	ros::NodeHandle nh;
	ros::Publisher vel_pub;
	vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel_mux/input/navi", 1, true);
        ros::Publisher tag_location_x = nh.advertise<std_msgs::Float64>("/tag_location_x", 1, true);
        ros::Publisher tag_location_y = nh.advertise<std_msgs::Float64>("/tag_location_y", 1, true);

	ros::Subscriber sub_scan = nh.subscribe("/scan", 10, scanCallback);
	//unsure
	ros::Subscriber sub_odom = nh.subscribe("/odom", 10, odom_call_back);
	ros::Subscriber sub_tag = nh.subscribe("/tag_detections", 10, tag_call_back);
	
	int times_per_sec = 10;

	ros::Rate loop_rate(times_per_sec);
	geometry_msgs::Twist vel;
	vel.linear.x = 0;//.1;//linear velocity(m/s)
	vel.angular.z = 0;//angular velocity(rad/s)


	//bool done_scanning = false;

	while (ros::ok())
	{

		vel_pub.publish(vel);
		ros::spinOnce();
				
		vel.angular.z = 0.05;
		
		size_t num_tags_detected = detections.size();
		for(size_t i = 0; i < num_tags_detected; ++i)
		{
			if((detections[i].pose.pose.position.x) <= 0.05)
			{
				float theta = 2*asin(orientation_z);
				tags_x[detections[i].id].data = (detections[i].pose.pose.position.z-0.3)*cos(theta);
				tags_y[detections[i].id].data = (detections[i].pose.pose.position.z-0.3)*sin(theta);
				
			}

		}

		if(orientation_z >= 0.8)
		{
			//done_scanning = true;
			//vel.angular.z = 2;
			break;
		}

		//if(done_scanning && orientation_z<0.03 && orientation_z>-0.03)
		//{
		//	break;
		//}			
		loop_rate.sleep();

	}

	std::cout<<0<<" "<<0<<std::endl;

	for(size_t i = 0; i<5; ++i)
	{
		tag_location_x.publish(tags_x[i]);
		tag_location_y.publish(tags_y[i]);
		ros::spinOnce();
		
		std::cout<<tags_x[i].data<<" "<<tags_y[i].data<<std::endl;

		loop_rate.sleep();
	}

	return 0;
}


