// Proof 1: Our radius of reaction works
// Proof 2: Similar triangles proof
// Proof 3: If position is left, it's going right, vice versa (after collision detection)

#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Imu.h>
#include <kobuki_msgs/BumperEvent.h>
#include <iostream>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/LaserScan.h>
#include <vector>
#include <deque>
#include <math.h>
#include <std_msgs/Int32.h>




struct Goal
{
	int tag_id;
	double x;
	double y;
};

std::deque<Goal> goals(5);


// states
enum State {gtg, obstacle_detection_start, obstacle_avoidance};
enum State_gtg {turning, straight};

// distance which we maintain between us and target
float target_distance = 1.5;

// raw data from laser scan
std::vector<float> range_data(640);
// minimum range detected
float min_range = 100;
int min_index = 0;
int min_sector = 0;

float linear_velocity = 0;
float current_theta = 0;
float current_x = 0;
float current_y = 0;


void velCallback(const nav_msgs::Odometry::ConstPtr& data)
{

  	linear_velocity = data->twist.twist.linear.x;
	current_theta = 2*asin(data->pose.pose.orientation.z);
	current_x = data->pose.pose.position.x;
	current_y = data->pose.pose.position.y;
}


void scanCallback(const sensor_msgs::LaserScan::ConstPtr& data)
{
	// get data from laser scan
 	range_data = data->ranges;//[0-639]
	
	min_range = 100;
	min_index = 0;
	min_sector = 0;
	// find global minimum
	for(size_t i = 0; i < 640; ++i)
	{
		if(range_data[i]<min_range)
		{
			min_range = range_data[i];
			min_index = i;
		}
	
	}
	int sector_width = 20;
	// therefore, there are 32 sectors
	min_sector = floor(min_index/sector_width);
}


int main(int argc, char **argv)
{
	ros::init(argc, argv, "turtlebot_final");
	ros::NodeHandle nh;
	ros::Publisher vel_pub;
	vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel_mux/input/navi", 1, true);
	ros::Publisher gtg_pub = nh.advertise<std_msgs::Int32>("/gtg", 1, true);
	ros::Subscriber sub_scan = nh.subscribe("/scan", 10, scanCallback);
	//unsure
	ros::Subscriber sub_vel = nh.subscribe("/odom", 10, velCallback);


	// Read in ordered goals
	for(int i = 0; i < 5; ++i)
	{
		std::cin>>goals[i].tag_id>>goals[i].x>>goals[i].y;
		std::cout<<goals[i].tag_id<<goals[i].x<<goals[i].y<<std::endl;

	}

	std_msgs::Int32 goal_reached_id;


	int times_per_sec = 10;
	//float dt = 1.0/float(times_per_sec);
	ros::Rate loop_rate(times_per_sec);
	geometry_msgs::Twist vel;
	vel.linear.x = 0;//.1;//linear velocity(m/s)
	vel.angular.z = 0;//angular velocity(rad/s)
	
	// Initialize current Goal
	Goal current_goal = goals.front();
	goals.pop_front();
	

	//
	State state = gtg; 
	State_gtg state_gtg = turning;
	int time = 0;
	int start_sector = 0;
	int end_sector = 0;

	float theta_difference = 0.0;
	float theta_error = 0.0;
	float gamma = 0.0;
	float k_angular = 1;

	float dist_error = 0.0;
	while (ros::ok())
	{
		//cout<<"HERE"<<endl;
		vel_pub.publish(vel);
		ros::spinOnce();
		// If it is not, we turn and look for person first, all of that good shit

		// Can change this
		vel.linear.x = 0.2;
		vel.angular.z = 0.0;
		std::cout<<"STATE: " << state<<std::endl;
		if(state == gtg)
		{
			// TODO: Do Go to goal behaviour
			std::cout<<"Going to Goal "<<current_goal.tag_id<<std::endl;
			// Angle controller
			theta_difference = atan2((current_goal.y-current_y), (current_goal.x-current_x));
			theta_error = theta_difference - current_theta;
			gamma = k_angular * atan2(sin(theta_error), cos(theta_error));
			//std::cout<<"current_theta "<<current_theta;
			//std::cout<<"theta_error "<<theta_error<<std::endl;
			//std::cout<<"gamma "<<gamma<<std::endl;
			if((theta_error>0.1 || theta_error<-0.1) && state_gtg == turning)
			{
				vel.angular.z = gamma;
				vel.linear.x = 0;
			}
			else
			{
				state_gtg = straight;
				vel.angular.z = gamma;
				std::cout<<"HERE"<<std::endl;
				vel.linear.x = 0.2;
			}	

			// If goal has been reached, update next goal
			dist_error = sqrt( ((current_goal.x-current_x)*(current_goal.x-current_x)) + ((current_goal.y-current_y)*(current_goal.y-current_y)) );

			std::cout<<"x: "<<current_x<<std::endl;
			std::cout<<"y: "<<current_y<<std::endl;
			std::cout<<"dist_error: "<<dist_error<<std::endl;
			if(dist_error<0.4)
			{
				goal_reached_id.data = current_goal.tag_id;
				gtg_pub.publish(goal_reached_id);
				if(goals.empty())
				{
					return 0;
				}
				state_gtg = turning;
				std::cout<<"Updating Goal"<<std::endl;
				current_goal = goals.front();
				goals.pop_front();
			}		
			dist_error = dist_error - 0.4;

			// TODO: This logic needs to change, While following if dynamic obstacle starts to appear
			std::cout<<"min_range "<< min_range<<std::endl;
			std::cout<<"detection radius "<< std::min(dist_error,float(2.0)) <<std::endl;		
			if(min_range <= std::min(dist_error,float(2.0)) && state_gtg == straight)
			{
				state = obstacle_detection_start;
				time = 0;
				start_sector = min_sector;
			} 
			
		} // gtg
		else if(state == obstacle_detection_start)
		{
			++time;
			//sampled at half a second, so hz/2,(0 to 4 is 5)
			if(time == 4)
			{
				end_sector = min_sector;
				// if end sector is the same as the start sector, it will collide!!
				// So collision is detected and system should go to obstacle avoidance behaviour
				if(abs(start_sector - end_sector) <= 1)
				{
					state = obstacle_avoidance;
					
				}
				else
				{
					state = gtg;
				}

			}

		} // obstacle_detection_start
		else if(state == obstacle_avoidance)
		{
			std::cout<<"SEEN SECTOR: "<<min_sector<<std::endl;
			std::cout<<"MIN_ RAGNCE: "<<min_range<<std::endl;
			// if it is in the middle, we turn// 1 is on the right??
			if(end_sector<=15 && end_sector>=11)
			{
				vel.angular.z = -4;
				state = gtg;
				if(end_sector <= 15 && min_sector >= 28)
				{
					state = gtg;
				}
			}
			else if(end_sector>=16 && end_sector <=20)
			{
				vel.angular.z = 4;
				state = gtg;
				if(end_sector >= 16 && min_sector <= 3)
				{
					state = gtg;
				}

			}
			else
			{
				vel.linear.x = 0.15*0.2;
				// if the dynamic obstacle has passed us, we go back to follow state
				// right to left, left to right
				//state = gtg;
				dist_error = sqrt( ((current_goal.x-current_x)*(current_goal.x-current_x)) + ((current_goal.y-current_y)*(current_goal.y-current_y)) )-0.4 ;
				if((end_sector >= 21 && min_sector <= 10) || (end_sector <= 10 && min_sector >=21) || min_range>=std::min(dist_error,float(2.0)) )
				{
					state = gtg;
				}
			}
			
		} // obstacle_avoidance		
		else
		{
			state = gtg;
		}

					
		loop_rate.sleep();

	}

	return 0;
}


