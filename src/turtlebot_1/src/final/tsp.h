#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <iterator>
#include <algorithm>
#include <limits>
#include <utility>
#include <iomanip>
#include <deque>
using namespace std;
class tsp
{
    public :

    struct Node
    {
        double distance;
        double x;
        double y;
    };
    
    vector<vector<double> > distMatrix;
    vector<Node> nodes;    
    vector<int> path;
    deque<int> unused;
    vector<int> bestPath;
    double bestDist = 0.0;
    double distSoFar = 0.0;
    
    void route();
    void setDist();
    void setBestDist();
    void addNode0();
    void print();

    private :

    bool promising();
    double mstWeight();
    int minDistIndex(vector<Node> &nodes, deque<int> &unvisited);
};

void tsp::print()
{
    
    //cout << bestPath[0] << "\n";
    
    for(size_t i = bestPath.size() - 1; i > 0; --i)
    {
        cout << bestPath[i]-1 <<" "<< nodes[bestPath[i]].x<<" "<<nodes[bestPath[i]].y<< "\n";
    }
    cout << "The total path length is: " << bestDist << "\n";
}

bool tsp::promising()
{
    double mst = mstWeight();
    double toPATHstart = std::numeric_limits<double>::infinity();
    double toPATHend = std::numeric_limits<double>::infinity();

    for( int i = 0; i < (int)unused.size(); ++i)
    {
        if(distMatrix[path.front()][unused[i]] < toPATHstart)
        {
            toPATHstart = distMatrix[path.front()][unused[i]];  
        }
        if(distMatrix[path.back()][unused[i]] < toPATHend)
        {
            toPATHend = distMatrix[path.back()][unused[i]];
        }
    }

    double estimate = mst + toPATHstart + toPATHend + distSoFar;
    return estimate < bestDist;
}

double tsp::mstWeight()
{    
    deque<int> unvisited = unused;
    for(int i = 0; i < (int)unvisited.size(); ++i)
    {
        nodes[unvisited[i]].distance = distMatrix[unvisited[0]][unvisited[i]];
    }
    //start by visiting 0, pop 0 out of unvisited
    unvisited.erase(unvisited.begin());
    double totalDist = 0.0;
    
    for(int j = 0; j < (int)(unused.size() -1); ++j)
    {
        //inner loop
        int minIndex = minDistIndex(nodes, unvisited);
        int base = unvisited[minIndex];
        totalDist += nodes[base].distance;
        unvisited.erase(unvisited.begin() + minIndex);
        
        for(int i = 0; i < (int)unvisited.size(); ++i)
        {
            double distance = distMatrix[unvisited[i]][base];
            if(distance < nodes[unvisited[i]].distance)
            {
                nodes[unvisited[i]].distance = distance;
            }
        }
    }
    return totalDist;
}


int tsp::minDistIndex(vector<Node> &nodes, deque<int> &unvisited)
{
    double minDist = nodes[unvisited[0]].distance;
    int minIndex = 0;
    for(int i = 0; i < (int)unvisited.size(); ++i)
    {
        if(nodes[unvisited[i]].distance < minDist)
        {
            minDist = nodes[unvisited[i]].distance;
            minIndex = i;
        }
    }
    return minIndex;
}

void tsp::addNode0()
{
    path.push_back(unused.front());
    unused.pop_front();
}

void tsp::setBestDist()
{
    for(int i = 0; i < (int)(bestPath.size()-1); ++i)
    {
        bestDist += distMatrix[bestPath[i]][bestPath[i+1]];
    }
    bestDist += distMatrix[bestPath.front()][bestPath.back()];
}

void tsp::setDist()
{
    for(int i = 0; i < (int)distMatrix.size(); ++i)
    {
        for(int j = 0; j <= i; ++j)
        {
            if(i == j)
            {
                distMatrix[i][j] = std::numeric_limits<double>::infinity();
            }
            else
            {
                distMatrix[i][j] = sqrt(pow((nodes[i].x - nodes[j].x), 2) + pow((nodes[i].y - nodes[j].y), 2));
                distMatrix[j][i] = distMatrix[i][j];    
            }
        }
    }
}

void tsp::route()
{

    double edge;
    if(unused.empty())
    {
        bestPath = path;
        bestDist = distSoFar + distMatrix[bestPath.front()][bestPath.back()];
        return;
    }
    if(!promising())
    {
        return;
    }
    for(int k = 0; k != (int)unused.size(); ++k)
    {
        path.push_back(unused.front());
        edge = distMatrix[path.back()][path[path.size()-2]];
        distSoFar += edge;
        
        unused.pop_front();
        route();
        unused.push_back(path.back());
        path.pop_back();
        distSoFar -= edge;
    }
}
