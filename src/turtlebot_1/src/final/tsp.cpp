#include <getopt.h>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <iterator>
#include <algorithm>
#include <limits>
#include <utility>
#include <iomanip>
#include "tsp.h"

using namespace std;

bool path();


int main(int argc, char *argv[])
{    
    ios_base::sync_with_stdio(false);
    cout << std::setprecision(2);
    cout << std::fixed;
    return path();

}


bool path()
{
    int N = 6;
    
    tsp TSP;

    TSP.nodes.resize(N);
    TSP.bestPath.reserve(N);
    TSP.unused.resize(N);

    for(int i = 0; i < N; ++i)
    {
        cin >> TSP.nodes[i].x >> TSP.nodes[i].y;
        TSP.unused[i] = i;
    } 

    // Hueristic for best path is just 0,1,2,3,4
    for(int i = 0; i < N; ++i)
    {
        TSP.bestPath.push_back(i);
    }

    
    TSP.distMatrix.resize(N, vector<double>(N));
    TSP.setDist();
    TSP.setBestDist();
    TSP.addNode0();    
    TSP.route();
    TSP.print();
    return 0;    
}



