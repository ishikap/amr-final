#include <ros/ros.h>
#include <geometry_msgs/Twist.h>#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Imu.h>
#include <math.h>
#include <sensor_msgs/Imu.h>
#include <math.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>

int main(int argc, char** argv)
{
  ros::init(argc, argv, "odometry_publisher");
  ros::NodeHandle nh;
  ros::Publisher vel_pub;
  vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel_mux/input/navi", 1, true);

  ros::Publisher odom_pub;
  odom_pub = nh.advertise<nav_msgs::Odometry>("odom",50)
  tf::TransformBroadcaster odom_broadcaster;

  ros::Subscriber sub = nh.subscribe("/mobile_base/sensors/imu_data", 10, imuCallback);
  
  /*
  float hz = 10;
  ros::Rate loop_rate(hz);
  int count = 0;
  geometry_msgs::Twist vel;
  vel.linear.x = 0;//linear velocity(m/s)
  vel.angular.z = 0;//angular velocity(rad/s)
  */

  
  // Initialize postion in the odom frame
  double x = 0.0;
  double y = 0.0;
  double theta = 0.0; // theta

  // Initialize velocity in the odom frame
  double vel_x = 0.1;
  double vel_y = -0.1;
  double vel_theta = 0.1;


  // Initialize time
  ros::Time current_time,last_time;
  current_time = ros::Time::now();
  last_time = ros::Time::now();

  ros::Rate r(1.0); // rate of the computation

  while(ros::ok())
  {
	ros::spinOnce();
	current_time = ros:Time::now();

	// Calculate the change in each variable and update them
	double dt = (current_time - last_time).toSec();  // toSec converts the ros::Time::now object into a double representation in seconds
	double delta_x = (vel_x * cos(theta) - vel_y * sin(theta)) * dt;
	double delta_y = (vel_x * sin(theta) + vel_y * cos(theta)) * dt;
	double delta_theta = vel_theta * dt;

	x += delta_x;
	y += delta_y;
	theta += delta_theta;


	// Make everything into a 3D message so that 2D and 3D components can work together - convert rotation into a Quaternion called odom_quat
	geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgsFromYam(theta);

	// Publish the Transform over tf
	geometry_msgs::TransformStamped odom_trans;

	odom_trans.header.stamp = current_time; // want the header frame to be related to the child frame at the current time 
	odom_trans.header.frame_id = "odom"; // name the header
	odom_trans.child.frame_id = "base_link"; // name the child

	// Do the Transformation
	odom_trans.transform.translation.x = x;
	odom_trans.transform.translation.y = y;
	odom_trans.transform.translation.z = 0.0;
	odom_trans.transform.rotation      = odom_quat; // need to define above as a Quaternion


	// Broadcast the Tranform to tf
	odom_broadcaster.sendTransform(odom_trans);

	// Publish odom over ROS
	nav_msgs::Odometry = odom;
	odom.header.stamp = current_time;
	odom.header.frame_id = "odom";


	// Set the position
	odom.pose.pose.position.x = x;
	odom.pose.pose.position.y = y;
	odom.pose.pose.position.z = 0.0;
	odom.pose.pose.orientation = odom_quat;

	// Set the velocity
	odom.child_frame_id = "base_link";
	odom.twist.twist.linear.x = vel_x;
	odom.twist.twist.linear.y = vel_y;
	odom.twist.twist.angular.z = vel_theta;

		std::cout<<"speed in x "<<vel_x<<std::endl;
		std::cout<<"speed in y "<<vel_y<<std::endl;
		std::cout<<"angular speed "<<vel_theta<<std::endl;
		std::cout<<"odom_quat "<<odom_quat<<std::endl;	
	
	// Publish the message 
	odom_pub.publish(odom)

	last_time = current_time;
	r.sleep();
  }
}
