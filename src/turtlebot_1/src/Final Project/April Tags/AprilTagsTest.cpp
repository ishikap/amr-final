#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Imu.h>
#include <math.h>

#include "apriltags2_ros/common_functions.h"
#include <sensor_msgs/Image.h>
// Tag Detector
#include "TagDetector.h"
#include "TagDetection.h"
#include "TagFamily.h"

// April tags detector and various families that can be selected by command line option
#include "AprilTags/Tag36h11.h"
#include "AprilTags/Tag25h9.h" // in case we want to use another family of tags

using namespace std;




AprilTags::TagCodes m_tagCodes;
AprilTags::TagDetector* m_tagDetector;
// string window_name = "Lilly's view" // TODO

/*
void setTagCodes(string s)
{
    if (s=="36h11") {
        m_tagCodes = AprilTags::tagCodes36h11;
    }
    //else if (s=="25h9") // for two April tag families
}
 */

/*
void setup()
{
    AprilTags::TagCodes m_tagCodes;
    m_tagCodes(AprilTags::tagCodes36h11);
    
    AprilTags::TagDetector* m_tagDetector;
    m_tagDetector = new AprilTags::TagDetector(m_tagCodes);
    
    
    bool m_draw
    m_draw(true);
    
    double m_tagSize;
    m_tagSize(0.166);
    
    
    // prepare window for drawing the camera images
    if (m_draw) {
        cv::namedWindow("Lilly's view", 1);
    }
}
 */

/*

void cameraCallback(const sensor_msgs::CameraInfoConstPtr& cam_info ){
    
}
 
*/


    
    


void aprilCallback(const sensor_msys::Image::ConstPtr& msg, const sensor_msgs::CameraInfoConstPtr& camera_info)
{
    // msg = image coming in from camera
    cv_bridge::CvImagePtr cv_ptr;
    cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    
    // Convert image to gray scale
    cv::Mat image_gray;
    cv::cvtColor(cv_ptr->image, image_gray, CV_BGR2GRAY);
    
    // Get April Tag detections
    vector<AprilTags::TagDetection> detections = m_tagDetector->extractTags(image_gray);    // detecting april tags requires a gray scale image
    
    
    // Help with converting images between OpenCv to Ros
    // http://wiki.ros.org/cv_bridge/Tutorials/UsingCvBridgeToConvertBetweenROSImagesAndOpenCVImages
    

    /*
    // show the current image including any detections
    if (m_draw) {
        for (int i=0; i<detections.size(); i++) {
            // also highlight in the image
            detections[i].draw(image);
        }
        imshow(window_name, image); // OpenCV call
    }
    */
    
    
    
    // Create instances for publishing
    AprilTagDetectionArray tag_detection_array;
    geometry_msgs::PoseArray tag_pose_array;
    tag_pose_array.header = cv_ptr->header; // need to learn what exactly is the 'tag_pose_array' and why is needs a header
    
    
    

    // TODO: Camera_info
    
    // http://docs.ros.org/melodic/api/sensor_msgs/html/msg/CameraInfo.html
    
    double fx;
    double fy;
    double px;
    double py;
    if (projected_optics_) {
        // use projected focal length and principal point
        // these are the correct values
        fx = camera_info->P[0];
        fy = camera_info->P[5];
        px = camera_info->P[2];
        py = camera_info->P[6];
    } else {
        // use camera intrinsic focal length and principal point
        // for backwards compatability
        fx = camera_info->K[0];
        fy = camera_info->K[4];
        px = camera_info->K[2];
        py = camera_info->K[5];
    }
    
    if(!sensor_frame_id_.empty())
        cv_ptr->header.frame_id = sensor_frame_id_;
    
    
    
    
    
    
    // if loop
    
    if (detections.size == 1){
        AprilTags::TagDetection detection = detections[0];
        
        
        // TODO: descriptions
        map<int, AprilTagDescription>::const_iterator description_itr = descriptions_.find(detection.id);
        AprilTagDescription description = description_itr->second;
        double tag_size = description.size(); //0.166;
        
        detection.draw(cv_ptr->image);
        Eigen::Matrix4d transform = detection.getRelativeTransform(tag_size, fx, fy, px, py);
        Eigen::Matrix3d rot = transform.block(0, 0, 3, 3);
        Eigen::Quaternion<double> rot_quaternion = Eigen::Quaternion<double>(rot);
        
        // http://docs.ros.org/hydro/api/apriltags/html/structAprilTags_1_1TagDetection.html#a137dfa567de4b8dc8f5d6c317e9c8458
        
        geometry_msgs::PoseStamped tag_pose;
        tag_pose.pose.position.x = transform(0, 3);
        tag_pose.pose.position.y = transform(1, 3);
        tag_pose.pose.position.z = transform(2, 3);
        tag_pose.pose.orientation.x = rot_quaternion.x();
        tag_pose.pose.orientation.y = rot_quaternion.y();
        tag_pose.pose.orientation.z = rot_quaternion.z();
        tag_pose.pose.orientation.w = rot_quaternion.w();
        tag_pose.header = cv_ptr->header;
        
        AprilTagDetection tag_detection;
        tag_detection.pose = tag_pose;
        tag_detection.id = detection.id;
        tag_detection.size = tag_size;
        tag_detection_array.detections.push_back(tag_detection);
        tag_pose_array.poses.push_back(tag_pose.pose);
        
        tf::Stamped<tf::Transform> tag_transform;
        tf::poseStampedMsgToTF(tag_pose, tag_transform);
        tf_pub_.sendTransform(tf::StampedTransform(tag_transform, tag_transform.stamp_, tag_transform.frame_id_, description.frame_name()));
        
        
        detection.draw(image);
        imshow("Lilly's view", image);
        
    }
    else{
        cout << "Error: multiple tags        Number of detections = " << detections.size() << endl;
    }
    
    
    
    

    
    
    /*
    // for loop
        
    for (i = 0, i < detections.size(), i++){
        AprilTags::TagDetection detection = detections[i];
        
        // code goes here for multiple detections
        
        detection.draw(image);
    
    }
    
    imshow("Lilly's view", image);
    
    */
    
    
    
    // Publish Detections
    detections_pub_.publish(tag_detection_array);
    
    // Publish the Tag Pose
    pose_pub_.publish(tag_pose_array);
    
    // Publish Image
    image_pub_.publish(cv_ptr->toImageMsg());
    
}




int main(int argc, char **argv)
{
    ros::init(argc, argv, "april_tags");
    ros::NodeHandle nh;
    
    // Setup
    m_tagCodes(AprilTags::tagCodes36h11);
    m_tagDetector = new AprilTags::TagDetector(m_tagCodes);
    
    /*
    bool m_draw
    m_draw(true);
    
    // prepare window for drawing the camera images
     if (m_draw) { cv::namedWindow("Lilly's view", 1); }
     */
    
    cv::namedWindow("Lilly's view", 1);

    
    
    // Subscribe to get the april tag
    ros::Subscriber image_sub = nh.subscribeCamera("image_rect", 1, aprilCallback);
    ros::Publisher  image_pub = nh.advertise("tag_detections_image", 1);
    ros::Publisher  detections_pub = nh.advertise<AprilTagDetectionArray>("tag_detections", 1);
    ros::Publisher  pose_pub = nh.advertise<geometry_msgs::PoseArray>("tag_detections_pose", 1);
    
    // http://wiki.ros.org/apriltags2_ros
    
    
    while (ros::ok())
    {
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}





    
    
    
    
    
    
    
    
    
    
    

// Callback for camera info
void InfoCallback(const sensor_msgs::CameraInfoConstPtr& camera_info)
{
    camera_info_ = (*camera_info);
    has_camera_info_ = true;
}


// Get the image
cv_bridge::CvImagePtr subscribed_ptr;

cv::Mat subscribed_gray = subscribed_ptr->image;
cv::Point2d opticalCenter;


// Detect AprilTag markers in the image
TagDetectionArray detections;
detector_->process(subscribed_gray, opticalCenter, detections);
visualization_msgs::MarkerArray marker_transforms;
apriltags::AprilTagDetections apriltag_detections;
apriltag_detections.header.frame_id = msg->header.frame_id;
apriltag_detections.header.stamp = msg->header.stamp;

cv_bridge::CvImagePtr subscribed_color_ptr;



// Fill in AprilTag detection.
apriltags::AprilTagDetection apriltag_det;
apriltag_det.header = marker_transform.header;
apriltag_det.id = marker_transform.id;
apriltag_det.tag_size = tag_size;
apriltag_det.pose = marker_transform.pose;
const TagDetection &det = detections[i];


marker_publisher_ = node_->advertise<visualization_msgs::MarkerArray>(
                                                                      DEFAULT_MARKER_TOPIC, 1, connect_callback,
                                                                      disconnect_callback);
apriltag_publisher_ = node_->advertise<apriltags::AprilTagDetections>(
                                                                      DEFAULT_DETECTIONS_TOPIC, 1, connect_callback, disconnect_callback);




    
    
    
    
    
    
    
    
    




AprilTags::TagDetector* m_tagDetector;
AprilTags::TagCodes m_tagCodes;

bool m_draw; // draw image and April tag detections?

// Initialize
m_tagDetector(NULL),
m_tagCodes(AprilTags::tagCodes36h11),

m_draw(true),


// show the current image including any detections
if (m_draw) {
    for (int i=0; i<detections.size(); i++) {
        // also highlight in the image
        detections[i].draw(image);
    }
    imshow(window_name, image); // OpenCV call
    }
