// The code below changes the direction of the robot randomly if it bumps into something

#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Imu.h>
#include <kobuki_msgs/BumperEvent.h>
#include <iostream>
#include <stdlib.h>
#include <sensor_msgs/LaserScan.h>

int pressed = 0;

void bumperCallback(const kobuki_msgs::BumperEvent::ConstPtr& data)
{
	if(data->state == kobuki_msgs::BumperEvent::PRESSED) {
  		ROS_INFO_STREAM("Pressed");
		pressed = 1;
	}
	else {
  		ROS_INFO_STREAM("Released");
		pressed = 0;
	}

}
void cliffCallback(const kobuki_msgs::SensorState::ConsPtr& data)
{
	right = data->bottom[0];
	center = data->bottom[1];
	left = data->bottom[2];
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "turtlebot_turn")
  ros::NodeHandle nh;
  ros::Publisher vel_pub;
  vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel_mux/input/navi", 1, true);
  ros::Subscriber sub_bumper = nh.subscribe("/mobile_base/sensors/core", 10, cliffCallback);
  //bumper switch to stop at the end
  ros::Rate loop_rate(10);
  int count = 0;
  geometry_msgs::Twist vel;
  vel.linear.x = 0.1;
  vel.angular.z = 0;
  





  printf("BUMPER MAIN STARTED");
  ros::init(argc, argv, "turtlebot_turn");
  ros::NodeHandle nh;
  ros::Publisher vel_pub;
  vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel_mux/input/navi", 1, true);
  ros::Subscriber sub_bumper = nh.subscribe("/mobile_base/events/bumper", 10, bumperCallback);
  ros::Subscriber sub = nh.subscribe("/scan", 10, scanCallback);

  ros::Rate loop_rate(10);
  int count = 0;
  geometry_msgs::Twist vel;
  vel.linear.x = 0.1;//linear velocity(m/s)
  vel.angular.z = 0;//angular velocity(rad/s)
  while (ros::ok())
  {
	
  	vel_pub.publish(vel);
    	ros::spinOnce();
	
    	loop_rate.sleep();
	// if bumper is pressed
	if(pressed==1)
	{
		vel.linear.x = -0.1;
		for (int i = 0; i < 5; ++i) 	
		{
    			loop_rate.sleep();
  			vel_pub.publish(vel);
		}
		
		vel.linear.x = 0;
		vel.angular.z = 0.5;

		for (int i = 0; i < (rand()%15 +15); ++i) 	
		{
    			loop_rate.sleep();
  			vel_pub.publish(vel);
		}
		vel.linear.x = 0.1;
		vel.angular.z = 0;

	}
  }

  return 0;
}


