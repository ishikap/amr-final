#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Imu.h>
#include <math.h>



void aprilCallback(const geometry_msgs::PoseArray::ConstPtr& aprilData)
{
	aprilData->linear.x; //poses[0].position.x
	aprilData->linear.y; //poses[0].position.y
	aprilData->angular.z; //poses[0].position.z

	ROS_INFO("aprilData=[%f, %f, %f]",  aprilData->poses[0].position.x,
                                                aprilData->poses[0].position.y,
                                                aprilData->poses[0].position.z);

}




int main(int argc, char **argv)
{
  ros::init(argc, argv, "turtlebot_pose");
  ros::NodeHandle nh;
  ros::Publisher vel_pub;
  vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel_mux/input/navi", 1, true);
  ros::Subscriber april_pose = nh.subscribe<<geometry_msgs::PoseArray>("/tag_detections_pose", 1000, aprilCallback);
  float hz = 10;
  ros::Rate loop_rate(hz);
  int count = 0;
  geometry_msgs::Twist vel;
  vel.linear.x = 0;//linear velocity(m/s)
  vel.angular.z = 0;//angular velocity(rad/s)

	
  while (ros::ok())
  {
	vel_pub.publish(vel);
    	ros::spinOnce();
	loop_rate.sleep();
	    		
	
  }

  return 0;
}april
