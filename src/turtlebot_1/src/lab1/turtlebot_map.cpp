/*

when tick starts to get around 62047 the velocity starts to act funky. The current velocity and the distance both get lower and eventually reach 0. 


*/

// The code below changes the direction of the robot randomly if it bumps into something

#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Imu.h>
#include <kobuki_msgs/BumperEvent.h>
#include <kobuki_msgs/SensorState.h>
#include <sensor_msgs/Joy.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <vector>
#include <rosbag/bag.h>
#include <std_msgs/Int32.h>
#include <std_msgs/String.h>


int pressed = 0;
float accel = 0;
float speeds[4] = {0.3, 0.5, 0.6, 0};
float tick = 0;
std::vector<float> direction(8); 
std::vector<int> speed(11); 

void tickCallback(const kobuki_msgs::SensorState::ConstPtr& data)
{
  tick  = data->left_encoder; // encoder accumulates to much, we need to find a way to reset the ecoder back down to 0 when it loops
  //ROS_INFO("Linear  Velocity = %f", data->linear.x);//print out the angular velocity
}

void imuCallback(const sensor_msgs::Imu::ConstPtr& data)
{
  //vel_current = data->linear.x;
  //ROS_INFO("Linear  Velocity = %f", data->linear.x);//print out the angular velocity
}

void bumperCallback(const kobuki_msgs::BumperEvent::ConstPtr& data)
{
	if(data->state == kobuki_msgs::BumperEvent::PRESSED) {
  		ROS_INFO_STREAM("Pressed");
		pressed = 1;
	}
	else {
  		ROS_INFO_STREAM("Released");
		pressed = 0;
	}
}

void joyCallback(const sensor_msgs::Joy::ConstPtr& data)
{
	direction = data->axes; // 
	speed = data->buttons; // 1 or 0

}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "turtlebot_map");
  ros::NodeHandle nh;
  ros::Publisher vel_pub;
  vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel_mux/input/navi", 1, true);
  ros::Subscriber sub_bumper = nh.subscribe("/mobile_base/events/bumper", 10, bumperCallback);
  ros::Subscriber sub = nh.subscribe("/mobile_base/sensors/imu_data", 10, imuCallback);
  ros::Subscriber sub_tick = nh.subscribe("/mobile_base/sensors/core", 10, tickCallback);
  ros::Subscriber sub_joy = nh.subscribe("/joy", 10, joyCallback);
  
  float hz = 10;
  float R = 0.038;
  float N = 2570;
  float d = 0;
  float tick_old = 0;

  ros::Rate loop_rate(hz);
  float tickmax = 0; // this value has a max limit of 65513
  geometry_msgs::Twist vel;
  vel.linear.x = 0.4;//linear velocity(m/s)
  vel.angular.z = 0;//angular velocity(rad/s)
  

  // New stuff for mapping to bag data
  //rosbag::Bag bag;
  //bag.open("test.bag", rosbag::bagmode::Write);
  
  //std_msgs::String str;
  //str.data = std::string("foo");

  //str_msgs::Float32 i;
  //i.data = 42;


  std::ofstream bag;
  bag.open("map_data.csv");

  float current_theta = 0;





  int start = 1;
  while (ros::ok())
  {
  	vel_pub.publish(vel);

	ros::spinOnce();
	loop_rate.sleep();
	
	std::cout << "tick_old: " << tick_old << " , tick: " << tick<< std:: endl;
	if(tick_old == 0 && tick!=0 && start == 1)
	{
		tick_old = tick;
		start = 0;
	}

	if(tick<tick_old) 
	{
		d = 2.0*3.14*R*((65513-tick_old) + tick)/N;
	}
	else if(tick>tick_old && tick_old==0)
	{
		d = 0;
	}
	else 
	{
		d = 2.0*3.14*R*(tick-tick_old)/N;
	}
	
  	std::cout<< "dist = " << d  << std::endl;
	std::cout<< "tickmax = " << tickmax  << std::endl;
	std::cout<< "tick = " << tick  << std::endl;

	tick_old = tick;

	// if bumper is pressed
	if(pressed==1)
	{
		bag<<d<<","<<current_theta<<std::endl;
		
		vel.linear.x = -0.1;
		for (int i = 0; i < 5; ++i) 	
		{
    			loop_rate.sleep();
  			vel_pub.publish(vel);
			tick_old = 0;
		}
		
		vel.linear.x = 0;
		vel.angular.z = 0.5;

		for (int i = 0; i < (rand()%15 +15); ++i) 	
		{
    			loop_rate.sleep();
  			vel_pub.publish(vel);
			current_theta += vel.angular.z/hz;
		}
		vel.linear.x = 0.4;
		vel.angular.z = 0;
	}
  }

  bag.close();
  return 0;

}


