source ~/turtlebot_ws/devel/setup.bash

# Sweep to scan AprilTags
echo "Starting AprilTag Detection"
rosrun turtlebot_1 tag_detection > tag_detect.out
echo $?
echo "Done detecting AprilTags, back to original configuration"

# Find path using TSP
echo "Finding Best Path"
rosrun turtlebot_1 tsp < tag_detect.out > tsp.out
echo $?
echo "Path found"
echo "Starting Navigation"
rosrun turtlebot_1 final < tsp.out
